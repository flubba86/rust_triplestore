#![feature(dropck_parametricity)]
#![feature(const_fn)]
#![feature(alloc)]
#![feature(heap_api)]
#![feature(coerce_unsized)]
#![feature(collections_bound)]
#![feature(unique)]
#![feature(thread_local)]
#![feature(drop_types_in_const)]
#![feature(associated_type_defaults)]
#![feature(plugin)]

#![plugin(clippy)]

extern crate alloc;
pub mod btree;
use btree::map::BTreeMap;
use btree::set::BTreeSet;
use std::rc::{Rc, Weak};
use std::cmp::{Ord};
use std::cell::RefCell;
use std::vec::Vec;
use std::fmt;
use std::collections::HashMap;


#[derive(Copy, Clone, Debug, Ord, PartialOrd, Eq, PartialEq)]
pub struct IndexKeyQuad<T: Copy + Clone>(T, T, T, T);

pub trait IndexKeyT {
	type A;
	type B: Copy+Clone;
    fn order_compare_pairs(&self,
                           s_pair: (Weak<Self::A>, Weak<Self::A>),
                           p_pair: (Weak<Self::A>, Weak<Self::A>),
                           o_pair: (Weak<Self::A>, Weak<Self::A>),
                           g_pair: (Weak<Self::A>, Weak<Self::A>))
                           -> [Option<(Weak<Self::A>, Weak<Self::A>)>; 4];
    fn get_compare_guids_tuple(&self) -> (Self::B, Self::B, Self::B, Self::B);
    fn create_from_guids(s: Self::B, p:Self::B, o:Self::B, g:Self::B) -> Self;
}

#[derive(Copy, Clone, Debug)]
pub struct OrdAndEqIndex<T: IndexKeyT>(T);

pub mod rdf_triple_store {
    use {IndexKeyT, IndexKeyQuad, OrdAndEqIndex, TripleStoreT, ThreadLocalGlobal, GuidManager};
    use std::fmt;
    use std::cmp::{Ord, Ordering};
    use std::rc::{Weak};
    use std::convert::{Into, From};
    use std::cell::{RefCell, Ref, RefMut};
    use std::ops::{Add};
    use btree::set::BTreeSet;
	use std::collections::HashMap;
	use std::marker::PhantomData;
	
    #[thread_local]
	pub static mut rdf_identifier_guid_manager: ThreadLocalGlobal<GuidManager<RDFIdentifier, RDFUnit>> =
    ThreadLocalGlobal(RefCell::new(GuidManager {
                          next_guid: None,
                          obj_to_guid: None,
                          guid_to_objref: None,
                      }),
                      false);


    #[derive(Debug, Clone)]
    pub struct RDFIdentifier(String);
    impl fmt::Display for RDFIdentifier {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            // Write strictly the first element into the supplied output
            // stream: `f`. Returns `fmt::Result` which indicates whether the
            // operation succeeded or failed. Note that `write!` uses syntax which
            // is very similar to `println!`.
            write!(f, "{}", self.0)
        }
    }
    impl<'a> ::std::convert::From<&'a str> for RDFIdentifier {
        fn from(fr_string: &'a str) -> RDFIdentifier {
            RDFIdentifier(fr_string.into())
        }
    }
    impl ::std::convert::From<String> for RDFIdentifier {
        fn from(fr_string: String) -> RDFIdentifier {
            RDFIdentifier(fr_string)
        }
    }

    impl PartialEq for RDFIdentifier {
        fn eq(&self, rhs: &RDFIdentifier) -> bool {
            self.0.eq(&rhs.0)
        }
    }
    impl Eq for RDFIdentifier {}


    impl PartialOrd for RDFIdentifier {
        fn partial_cmp(&self, rhs: &RDFIdentifier) -> Option<Ordering> {
            self.0.partial_cmp(&rhs.0)
        }
    }
    impl Ord for RDFIdentifier {
        fn cmp(&self, rhs: &RDFIdentifier) -> Ordering {
            self.0.cmp(&rhs.0)
        }
    }

    #[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd)]
    pub struct RDFUnit(u64);
        impl fmt::Display for RDFUnit {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            // Write strictly the first element into the supplied output
            // stream: `f`. Returns `fmt::Result` which indicates whether the
            // operation succeeded or failed. Note that `write!` uses syntax which
            // is very similar to `println!`.
            write!(f, "{}", self.0)
        }
    }
    impl From<u64> for RDFUnit {
	    fn from(input: u64) -> RDFUnit {
		    RDFUnit(input)
	    }
    }   
    impl Into<u64> for RDFUnit {
	    fn into(self) -> u64 {
		    self.0
	    }
    }
    impl Add for RDFUnit {
    	type Output = RDFUnit;
	    fn add(self, other: RDFUnit) -> Self::Output {
	    	RDFUnit(self.0 + other.0)
	    }
    } 
    
    #[derive(Copy, Clone, Debug, Ord, PartialOrd, Eq, PartialEq)]
    pub struct IndexKeySPOG(IndexKeyQuad<RDFUnit>); /* s, p, o, g */
    #[derive(Copy, Clone, Debug, Ord, PartialOrd, Eq, PartialEq)]
    pub struct IndexKeyGP(IndexKeyQuad<RDFUnit>); /* g, p, s, o */
    #[derive(Copy, Clone, Debug, Ord, PartialOrd, Eq, PartialEq)]
    pub struct IndexKeyOGS(IndexKeyQuad<RDFUnit>); /* o, g, s, p */
    #[derive(Copy, Clone, Debug, Ord, PartialOrd, Eq, PartialEq)]
    pub struct IndexKeyPOG(IndexKeyQuad<RDFUnit>); /* p, o, g, s */
    #[derive(Copy, Clone, Debug, Ord, PartialOrd, Eq, PartialEq)]
    pub struct IndexKeyGSP(IndexKeyQuad<RDFUnit>); /* g, s, p, o */
    #[derive(Copy, Clone, Debug, Ord, PartialOrd, Eq, PartialEq)]
    pub struct IndexKeyOS(IndexKeyQuad<RDFUnit>); /* o, s, p, g */

	impl From<(RDFUnit, RDFUnit, RDFUnit, RDFUnit)> for IndexKeyQuad<RDFUnit> {
		fn from(input: (RDFUnit, RDFUnit, RDFUnit, RDFUnit)) -> IndexKeyQuad<RDFUnit> {
			IndexKeyQuad(input.0, input.1, input.2, input.3)
		}
	}

    impl IndexKeyT for IndexKeySPOG {
        type A = RDFIdentifier;
		type B = RDFUnit;
        fn order_compare_pairs(&self,
                               s_pair: (Weak<Self::A>, Weak<Self::A>),
                               p_pair: (Weak<Self::A>, Weak<Self::A>),
                               o_pair: (Weak<Self::A>, Weak<Self::A>),
                               g_pair: (Weak<Self::A>, Weak<Self::A>))
                               -> [Option<(Weak<Self::A>, Weak<Self::A>)>; 4] {
            [Some(s_pair), Some(p_pair), Some(o_pair), Some(g_pair)] /* S, P, O, G */
        }
        fn get_compare_guids_tuple(&self) -> (Self::B, Self::B, Self::B, Self::B) {
            ((self.0).0, (self.0).1, (self.0).2, (self.0).3)
        }
        fn create_from_guids(s: Self::B, p:Self::B, o:Self::B, g:Self::B) -> Self {
	        IndexKeySPOG((s, p, o, g).into())
        }
    }
    #[allow(unused_variables)]
    impl IndexKeyT for IndexKeyGP {
        type A = RDFIdentifier;
		type B = RDFUnit;
        fn order_compare_pairs(&self,
                               s_pair: (Weak<Self::A>, Weak<Self::A>),
                               p_pair: (Weak<Self::A>, Weak<Self::A>),
                               o_pair: (Weak<Self::A>, Weak<Self::A>),
                               g_pair: (Weak<Self::A>, Weak<Self::A>))
                               -> [Option<(Weak<Self::A>, Weak<Self::A>)>; 4] {
            [Some(g_pair), Some(p_pair), None, None]
        }
        fn get_compare_guids_tuple(&self) -> (Self::B, Self::B, Self::B, Self::B) {
            ((self.0).2, (self.0).1, (self.0).3, (self.0).0)
        }
        fn create_from_guids(s: Self::B, p:Self::B, o:Self::B, g:Self::B) -> Self {
	        IndexKeyGP((g, p, s, o).into())
        }
    }
    #[allow(unused_variables)]
    impl IndexKeyT for IndexKeyOGS {
        type A = RDFIdentifier;
		type B = RDFUnit;
        fn order_compare_pairs(&self,
                               s_pair: (Weak<Self::A>, Weak<Self::A>),
                               p_pair: (Weak<Self::A>, Weak<Self::A>),
                               o_pair: (Weak<Self::A>, Weak<Self::A>),
                               g_pair: (Weak<Self::A>, Weak<Self::A>))
                               -> [Option<(Weak<Self::A>, Weak<Self::A>)>; 4] {
            [Some(o_pair), Some(g_pair), Some(s_pair), None]
        }
        fn get_compare_guids_tuple(&self) -> (Self::B, Self::B, Self::B, Self::B) {
            ((self.0).2, (self.0).3, (self.0).0, (self.0).1)
        }
        fn create_from_guids(s: Self::B, p:Self::B, o:Self::B, g:Self::B) -> Self {
	        IndexKeyOGS((o, g, s, p).into())
        }
    }
    #[allow(unused_variables)]
    impl IndexKeyT for IndexKeyPOG {
        type A = RDFIdentifier;
		type B = RDFUnit;
        fn order_compare_pairs(&self,
                               s_pair: (Weak<Self::A>, Weak<Self::A>),
                               p_pair: (Weak<Self::A>, Weak<Self::A>),
                               o_pair: (Weak<Self::A>, Weak<Self::A>),
                               g_pair: (Weak<Self::A>, Weak<Self::A>))
                               -> [Option<(Weak<Self::A>, Weak<Self::A>)>; 4] {
            [Some(p_pair), Some(o_pair), Some(g_pair), None]
        }
        fn get_compare_guids_tuple(&self) -> (Self::B, Self::B, Self::B, Self::B) {
            ((self.0).3, (self.0).0, (self.0).1, (self.0).2)
        }
        fn create_from_guids(s: Self::B, p:Self::B, o:Self::B, g:Self::B) -> Self {
	        IndexKeyPOG((p, o, g, s).into())
        }
    }
    #[allow(unused_variables)]
    impl IndexKeyT for IndexKeyGSP {
        type A = RDFIdentifier;
		type B = RDFUnit;
        fn order_compare_pairs(&self,
                               s_pair: (Weak<Self::A>, Weak<Self::A>),
                               p_pair: (Weak<Self::A>, Weak<Self::A>),
                               o_pair: (Weak<Self::A>, Weak<Self::A>),
                               g_pair: (Weak<Self::A>, Weak<Self::A>))
                               -> [Option<(Weak<Self::A>, Weak<Self::A>)>; 4] {
            [Some(g_pair), Some(s_pair), Some(p_pair), None]
        }
        fn get_compare_guids_tuple(&self) -> (Self::B, Self::B, Self::B, Self::B) {
            ((self.0).1, (self.0).2, (self.0).3, (self.0).0)
        }
        fn create_from_guids(s: Self::B, p:Self::B, o:Self::B, g:Self::B) -> Self {
	        IndexKeyGSP((g, s, p, o).into())
        }
    }
    #[allow(unused_variables)]
    impl IndexKeyT for IndexKeyOS {
        type A = RDFIdentifier;
		type B = RDFUnit;
        fn order_compare_pairs(&self,
                               s_pair: (Weak<Self::A>, Weak<Self::A>),
                               p_pair: (Weak<Self::A>, Weak<Self::A>),
                               o_pair: (Weak<Self::A>, Weak<Self::A>),
                               g_pair: (Weak<Self::A>, Weak<Self::A>))
                               -> [Option<(Weak<Self::A>, Weak<Self::A>)>; 4] {
            [Some(o_pair), Some(s_pair), None, None]
        }
        fn get_compare_guids_tuple(&self) -> (Self::B, Self::B, Self::B, Self::B) {
            ((self.0).1, (self.0).2, (self.0).0, (self.0).3)
        }
        fn create_from_guids(s: Self::B, p:Self::B, o:Self::B, g:Self::B) -> Self {
	        IndexKeyOS((o, s, p, g).into())
        }
    }
    
    
	impl<T: IndexKeyT> PartialOrd for OrdAndEqIndex<T> where OrdAndEqIndex<T>: PartialEq, <T as IndexKeyT>::B: PartialEq+PartialOrd+Copy+Clone+Into<RDFUnit>+From<RDFUnit>, <T as IndexKeyT>::A: Eq+Ord+From<RDFIdentifier>+Into<RDFIdentifier>, Weak<<T as IndexKeyT>::A>: From<Weak<RDFIdentifier>>+Into<Weak<RDFIdentifier>> {
		fn partial_cmp(&self, rhs_type: &Self) -> Option<Ordering> {
			let lhs = self.0.get_compare_guids_tuple();
			let rhs = rhs_type.0.get_compare_guids_tuple();
			let any_match: <T as IndexKeyT>::B = TripleStore::get_guid_manager().get_anymatch_guid().into();
			let mut is_already_equal = true;
			for &(pair_l, pair_r) in &[(lhs.0, rhs.0),(lhs.1, rhs.1),(lhs.2, rhs.2), (lhs.3, rhs.3)] {
				if pair_l == any_match || pair_l == pair_r {
					continue;
				}
				is_already_equal = false;
				break;
			}
			if is_already_equal {
				return Some(Ordering::Equal);
			}
	
			let (s1, p1, o1, g1) = TripleStore::get_guid_manager().get_weak_objrefs(&(lhs.0.into(), lhs.1.into(), lhs.2.into(), lhs.3.into()));
			let (lhs_s, lhs_p, lhs_o, lhs_g) = (s1, p1, o1, g1);
			let (s2, p2, o2, g2) = TripleStore::get_guid_manager().get_weak_objrefs(&(rhs.0.into(), rhs.1.into(), rhs.2.into(), rhs.3.into()));
			let (rhs_s, rhs_p, rhs_o, rhs_g) = (s2, p2, o2, g2);
	
			for pair in &self.0.order_compare_pairs((lhs_s.into(), rhs_s.into()), (lhs_p.into(), rhs_p.into()), (lhs_o.into(), rhs_o.into()), (lhs_g.into(), rhs_g.into())) {
				match *pair {
					None => continue,
					Some(ref a) => {
						match a.0.upgrade().partial_cmp(&a.1.upgrade()) {
							None => return None,
							Some(b) => match b {
								Ordering::Equal => continue,
								Ordering::Less => return Some(Ordering::Less),
								Ordering::Greater => return Some(Ordering::Greater)
							}
						};
					}
				}
			}
			Some(Ordering::Equal)
		}
	}
	
	impl<T: IndexKeyT> Ord for OrdAndEqIndex<T> where OrdAndEqIndex<T>: Eq, <T as IndexKeyT>::B: Eq+Ord+Copy+Clone+Into<RDFUnit>+From<RDFUnit>, <T as IndexKeyT>::A: Eq+Ord+From<RDFIdentifier>+Into<RDFIdentifier>, Weak<<T as IndexKeyT>::A>: From<Weak<RDFIdentifier>>+Into<Weak<RDFIdentifier>> {
		fn cmp(&self, rhs_type: &Self) -> Ordering{
			let lhs = self.0.get_compare_guids_tuple();
			let rhs = rhs_type.0.get_compare_guids_tuple();
			let any_match: <T as IndexKeyT>::B = TripleStore::get_guid_manager().get_anymatch_guid().into();
			let mut is_already_equal = true;
			for &(pair_l, pair_r) in &[(lhs.0, rhs.0),(lhs.1, rhs.1),(lhs.2, rhs.2), (lhs.3, rhs.3)] {
				if pair_l == any_match || pair_l == pair_r {
					continue;
				}
				is_already_equal = false;
				break;
			}
			if is_already_equal {
				return Ordering::Equal;
			}
	
			let (s1, p1, o1, g1) = TripleStore::get_guid_manager().get_weak_objrefs(&(lhs.0.into(), lhs.1.into(), lhs.2.into(), lhs.3.into()));
			let (lhs_s, lhs_p, lhs_o, lhs_g) = (s1, p1, o1, g1);
			let (s2, p2, o2, g2) = TripleStore::get_guid_manager().get_weak_objrefs(&(rhs.0.into(), rhs.1.into(), rhs.2.into(), rhs.3.into()));
			let (rhs_s, rhs_p, rhs_o, rhs_g) = (s2, p2, o2, g2);
	
			for pair in &self.0.order_compare_pairs((lhs_s.into(), rhs_s.into()), (lhs_p.into(), rhs_p.into()), (lhs_o.into(), rhs_o.into()), (lhs_g.into(), rhs_g.into())) {
				match *pair {
					None => continue,
					Some(ref a) => {
						match a.0.upgrade().cmp(&a.1.upgrade()) {
							Ordering::Equal => continue,
							Ordering::Less => return Ordering::Less,
							Ordering::Greater => return Ordering::Greater
						};
					}
				}
			}
			Ordering::Equal
		}
	}
	
	#[derive(Debug, Eq, PartialEq)]
	pub struct TripleStore<A, B: Clone+Copy> {
		index_spog: BTreeSet<OrdAndEqIndex<IndexKeySPOG>>,
		index_gp: BTreeSet<OrdAndEqIndex<IndexKeyGP>>,
		index_ogs: BTreeSet<OrdAndEqIndex<IndexKeyOGS>>,
		index_pog: BTreeSet<OrdAndEqIndex<IndexKeyPOG>>,
		index_gsp: BTreeSet<OrdAndEqIndex<IndexKeyGSP>>,
		index_os: BTreeSet<OrdAndEqIndex<IndexKeyOS>>,
	    graphs: HashMap<String, B>,
	    default_graph: B,
	    _store_type: PhantomData<*const A>
	}
	
	impl TripleStore<RDFIdentifier, RDFUnit> {
		pub fn new() -> TripleStore<RDFIdentifier, RDFUnit> {
			let index_spog: BTreeSet<OrdAndEqIndex<<Self as TripleStoreT>::IndexSPOGType>> = BTreeSet::new();
	        let index_gp: BTreeSet<OrdAndEqIndex<<Self as TripleStoreT>::IndexGPType>> = BTreeSet::new();
	        let index_ogs: BTreeSet<OrdAndEqIndex<<Self as TripleStoreT>::IndexOGSType>> = BTreeSet::new();
	        let index_pog: BTreeSet<OrdAndEqIndex<<Self as TripleStoreT>::IndexPOGType>> = BTreeSet::new();
	        let index_gsp: BTreeSet<OrdAndEqIndex<<Self as TripleStoreT>::IndexGSPType>> = BTreeSet::new();
	        let index_os: BTreeSet<OrdAndEqIndex<<Self as TripleStoreT>::IndexOSType>> = BTreeSet::new();
	        
	        let mut new_ts = TripleStore {
	            index_spog: index_spog,
	            index_gp: index_gp,
	            index_ogs: index_ogs,
	            index_pog: index_pog,
	            index_gsp: index_gsp,
	            index_os: index_os,
	            default_graph: 0u64.into(),
	            graphs: HashMap::new(),
	            _store_type: PhantomData
	        };
	        let graph_id = new_ts.add_new_graph("DefaultGraph".into());
	        new_ts.default_graph = graph_id.unwrap();
	        new_ts
			
		}
	}
	
	impl<'a> TripleStoreT<'a> for TripleStore<RDFIdentifier, RDFUnit> {
	    type IdentifierType=RDFIdentifier;
	    type GuidType=RDFUnit;
	    type IndexSPOGType=IndexKeySPOG;
	    type IndexGPType=IndexKeyGP;
	    type IndexOGSType=IndexKeyOGS;
	    type IndexPOGType=IndexKeyPOG;
	    type IndexGSPType=IndexKeyGSP;
		type IndexOSType=IndexKeyOS;
		type P=GuidManager<RDFIdentifier, RDFUnit>;
	
	    fn get_guid_manager() -> Ref<'a, GuidManager<RDFIdentifier, RDFUnit>> {
	        let b;
	        unsafe {
	            if !rdf_identifier_guid_manager.is_initialized() {
	                rdf_identifier_guid_manager.borrow_mut().initialize();
	                rdf_identifier_guid_manager.set_initialized();
	            }
	            b = rdf_identifier_guid_manager.borrow()
	        }
	        b
	    }
	    fn get_guid_manager_mut() -> RefMut<'a, GuidManager<RDFIdentifier, RDFUnit>> {
	        let b;
	        unsafe {
	            if !rdf_identifier_guid_manager.is_initialized() {
	                rdf_identifier_guid_manager.borrow_mut().initialize();
	                rdf_identifier_guid_manager.set_initialized();
	            }
	            b = rdf_identifier_guid_manager.borrow_mut()
	        }
	        b
	    }
	    
	    fn borrow_graphmap(&self) -> &HashMap<String, RDFUnit> {
		    &self.graphs
	    }
	    
	    fn borrow_graphmap_mut(&mut self) -> &mut HashMap<String, RDFUnit> {
		    &mut self.graphs
	    }
	    
	    fn get_default_graph(&self) -> RDFUnit {
		    self.default_graph	
	    }
	    
	    fn print_resolver(&self) {
	        //Self::get_guid_manager().print();
	    }
	    
	    fn borrow_spog_index(&self) -> &BTreeSet<OrdAndEqIndex<IndexKeySPOG>> {
		    &self.index_spog
	    }
	    fn borrow_gp_index(&self) -> &BTreeSet<OrdAndEqIndex<IndexKeyGP>> {
		    &self.index_gp
	    }
	    fn borrow_ogs_index(&self) -> &BTreeSet<OrdAndEqIndex<IndexKeyOGS>> {
		    &self.index_ogs
	    }
	    fn borrow_pog_index(&self) -> &BTreeSet<OrdAndEqIndex<IndexKeyPOG>> {
		    &self.index_pog
	    }
	    fn borrow_gsp_index(&self) -> &BTreeSet<OrdAndEqIndex<IndexKeyGSP>> {
		    &self.index_gsp
	    }
	    fn borrow_os_index(&self) -> &BTreeSet<OrdAndEqIndex<IndexKeyOS>> {
		    &self.index_os
	    }
	    fn borrow_spog_index_mut(&mut self) -> &mut BTreeSet<OrdAndEqIndex<IndexKeySPOG>> {
		    &mut self.index_spog
	    }
	    fn borrow_gp_index_mut(&mut self) -> &mut BTreeSet<OrdAndEqIndex<IndexKeyGP>> {
		    &mut self.index_gp
	    }
	    fn borrow_ogs_index_mut(&mut self) -> &mut BTreeSet<OrdAndEqIndex<IndexKeyOGS>> {
		    &mut self.index_ogs
	    }
	    fn borrow_pog_index_mut(&mut self) -> &mut BTreeSet<OrdAndEqIndex<IndexKeyPOG>> {
		    &mut self.index_pog
	    }
	    fn borrow_gsp_index_mut(&mut self) -> &mut BTreeSet<OrdAndEqIndex<IndexKeyGSP>> {
		    &mut self.index_gsp
	    }
	    fn borrow_os_index_mut(&mut self) -> &mut BTreeSet<OrdAndEqIndex<IndexKeyOS>> {
		    &mut self.index_os
	    }
	}
}



#[derive(Clone, Debug)]
pub struct GuidManager<A: Ord + PartialOrd + Eq, B: Copy + Clone> {
    pub next_guid: Option<B>,
    pub obj_to_guid: Option<BTreeMap<Rc<A>, B>>,
    pub guid_to_objref: Option<Vec<Rc<A>>>,
}

unsafe impl<A: Ord + PartialOrd + Eq + Send + Sync, B: Copy + Clone> Sync for GuidManager<A, B> {}
unsafe impl<A: Ord + PartialOrd + Eq + Send + Sync, B: Copy + Clone> Send for GuidManager<A, B> {}

impl<A: Ord+PartialOrd+std::convert::From<String>, B: Copy+Clone+std::ops::Add+Into<u64>+From<u64>+From<<B as std::ops::Add>::Output>> GuidManager<A, B> {
	pub fn initialize(&mut self) {
		self.next_guid = Some(0u64.into());
		self.obj_to_guid = Some(BTreeMap::new());
		self.guid_to_objref = Some(Vec::new());
		self.make_new_guid(String::from("AnyMatch").into());
	}

	pub fn get_anymatch_guid(&self) -> B {
		0u64.into()
	}

	pub fn make_new_guid(&mut self, obj: A) -> B {
		let mut mapper = self.guid_to_objref.as_mut().unwrap();
		let mut back_mapper = self.obj_to_guid.as_mut().unwrap();
		let new_guid = self.next_guid.unwrap();
		let alloced_guid_space = mapper.len();
		let rc_obj = Rc::new(obj);

		if new_guid.into() as usize >= alloced_guid_space {
			mapper.push(rc_obj.clone());
			self.next_guid = Some((mapper.len() as u64).into());
		} else {
			mapper[new_guid.into() as usize] = rc_obj.clone();
			self.next_guid = Some((new_guid.into() + 1u64).into());
		}
		back_mapper.insert(rc_obj, new_guid);
		new_guid
	}

	pub fn check_existing_guid(&self, check_val: &A) -> Option<&B> {
		let back_mapper = self.obj_to_guid.as_ref().unwrap();
		back_mapper.get(check_val)
	}

	pub fn get_objref_if_exists(&self, check_val: &A) -> Option<Rc<A>> {
		match self.check_existing_guid(check_val) {
			None => return Option::None,
			Some(a) => { let mapper = self.guid_to_objref.as_ref().unwrap();
				         return mapper.get((*a).into() as usize).cloned() }
		};
	}

	pub fn get_or_add_guid(&mut self, add_val: A) -> B {
		{
			let existing_guid = self.check_existing_guid(&add_val);
			if existing_guid.is_some() { return *existing_guid.unwrap()};
		}
		self.make_new_guid(add_val)
	}
		
	pub fn lookup_weak_ref(&self, guid:B) -> Option<Weak<A>> {
		let mapper = self.guid_to_objref.as_ref().unwrap();
		match mapper.get(guid.into() as usize) {
			Some(a) => {
				Some(Rc::downgrade(a))
			},
			None => None
		}
		
	}
	
	pub fn lookup_strong_ref(&self, guid:B) -> Option<Rc<A>> {
		let mapper = self.guid_to_objref.as_ref().unwrap();
		mapper.get(guid.into() as usize).cloned()
	}			
		
	pub fn get_weak_objrefs(&self, guids:&(B, B, B, B)) -> (Weak<A>, Weak<A>, Weak<A>, Weak<A>)
	{
		let mapper = self.guid_to_objref.as_ref().unwrap();
		(
			Rc::downgrade(mapper.get(guids.0.into() as usize).unwrap()),
			Rc::downgrade(mapper.get(guids.1.into() as usize).unwrap()),
			Rc::downgrade(mapper.get(guids.2.into() as usize).unwrap()),
			Rc::downgrade(mapper.get(guids.3.into() as usize).unwrap())
		)
	}
	pub fn get_strong_objrefs(&self, guids:&(B, B, B, B)) -> (Rc<A>, Rc<A>, Rc<A>, Rc<A>)
	{
		let mapper = self.guid_to_objref.as_ref().unwrap();
		(
			mapper.get(guids.0.into() as usize).unwrap().clone(),
			mapper.get(guids.1.into() as usize).unwrap().clone(),
			mapper.get(guids.2.into() as usize).unwrap().clone(),
			mapper.get(guids.3.into() as usize).unwrap().clone()
		)
	}
}

trait GuidManagerPrinter<A> {
    fn print(&self);
}
impl<A: fmt::Display+fmt::Debug+PartialOrd+Ord+Eq, B: Copy+Clone+fmt::Display+fmt::Debug> GuidManagerPrinter<A> for GuidManager<A, B> {
	fn print(&self) {
		if self.obj_to_guid.is_none() { println!("obj_to_guid: None.") }
		else {
			for (k,v) in self.obj_to_guid.as_ref().unwrap().iter() {
				println!("{:?}={:?}",k,v);
			}
		}
	}
}

pub struct ThreadLocalGlobal<A: Send + Sync>(RefCell<A>, bool);

impl<A: Send + Sync> ThreadLocalGlobal<A> {
    fn borrow(&self) -> std::cell::Ref<A> {
        self.0.borrow()
    }
    fn borrow_mut(&self) -> std::cell::RefMut<A> {
        self.0.borrow_mut()
    }
    fn is_initialized(&self) -> bool {
        self.1
    }
    fn set_initialized(&mut self) {
        self.1 = true
    }
}

unsafe impl<A: Send + Sync> Sync for ThreadLocalGlobal<A> {}
unsafe impl<A: Send + Sync> Send for ThreadLocalGlobal<A> {}

trait TripleStoreT<'a> where OrdAndEqIndex<Self::IndexSPOGType>:Ord+Eq+fmt::Debug,
OrdAndEqIndex<Self::IndexGPType>:Ord+Eq+fmt::Debug,
OrdAndEqIndex<Self::IndexOGSType>:Ord+Eq+fmt::Debug,
OrdAndEqIndex<Self::IndexPOGType>:Ord+Eq+fmt::Debug,
OrdAndEqIndex<Self::IndexGSPType>:Ord+Eq+fmt::Debug,
OrdAndEqIndex<Self::IndexOSType>:Ord+Eq+fmt::Debug,
Self::IdentifierType: From<String>,
Self: 'a,
{
	type IdentifierType: PartialOrd+Ord+Eq;
	type GuidType: PartialOrd+Ord+Eq+Copy+Clone+From<u64>+Into<u64>+std::ops::Add+From<<Self::GuidType as std::ops::Add>::Output>;
	type IndexSPOGType: IndexKeyT<A=Self::IdentifierType, B=Self::GuidType>+Eq+Ord;
	type IndexGPType: IndexKeyT<A=Self::IdentifierType, B=Self::GuidType>+Eq+Ord;
	type IndexOGSType: IndexKeyT<A=Self::IdentifierType, B=Self::GuidType>+Eq+Ord;
	type IndexPOGType: IndexKeyT<A=Self::IdentifierType, B=Self::GuidType>+Eq+Ord;
	type IndexGSPType: IndexKeyT<A=Self::IdentifierType, B=Self::GuidType>+Eq+Ord;
	type IndexOSType: IndexKeyT<A=Self::IdentifierType, B=Self::GuidType>+Eq+Ord;
	
	type P: GuidManagerPrinter<Self::IdentifierType>;

	
    fn get_guid_manager() -> std::cell::Ref<'a, GuidManager<Self::IdentifierType, Self::GuidType>>;
    fn get_guid_manager_mut() -> std::cell::RefMut<'a, GuidManager<Self::IdentifierType, Self::GuidType>>;
    fn print_resolver(&self);
	fn borrow_spog_index(&self) -> &BTreeSet<OrdAndEqIndex<Self::IndexSPOGType>>;
	fn borrow_gp_index(&self) -> &BTreeSet<OrdAndEqIndex<Self::IndexGPType>>;
	fn borrow_ogs_index(&self) -> &BTreeSet<OrdAndEqIndex<Self::IndexOGSType>>;
	fn borrow_pog_index(&self) -> &BTreeSet<OrdAndEqIndex<Self::IndexPOGType>>;
	fn borrow_gsp_index(&self) -> &BTreeSet<OrdAndEqIndex<Self::IndexGSPType>>;
	fn borrow_os_index(&self) -> &BTreeSet<OrdAndEqIndex<Self::IndexOSType>>;
	fn borrow_spog_index_mut(&mut self) -> &mut BTreeSet<OrdAndEqIndex<Self::IndexSPOGType>>;
	fn borrow_gp_index_mut(&mut self) -> &mut BTreeSet<OrdAndEqIndex<Self::IndexGPType>>;
	fn borrow_ogs_index_mut(&mut self) -> &mut BTreeSet<OrdAndEqIndex<Self::IndexOGSType>>;
	fn borrow_pog_index_mut(&mut self) -> &mut BTreeSet<OrdAndEqIndex<Self::IndexPOGType>>;
	fn borrow_gsp_index_mut(&mut self) -> &mut BTreeSet<OrdAndEqIndex<Self::IndexGSPType>>;
	fn borrow_os_index_mut(&mut self) -> &mut BTreeSet<OrdAndEqIndex<Self::IndexOSType>>;
	
	fn borrow_graphmap(&self) -> &HashMap<String, Self::GuidType>;
	fn borrow_graphmap_mut(&mut self)-> &mut HashMap<String, Self::GuidType>;
	fn get_default_graph(&self) -> Self::GuidType;

    fn add_new_graph(&mut self, graph_uri: String) -> Option<Self::GuidType> {
        {
            // borrow self as immut for this block
            if let Some(a) = self.borrow_graphmap().get(&graph_uri) {
                return Some(*a);
            };
        } /* and return the borrow so that it can be borrowed as mut next */
        let new_identifier: Self::IdentifierType = graph_uri.clone().into();
        let new_graph_id: Self::GuidType;
        {
            new_graph_id = Self::get_guid_manager_mut().make_new_guid(new_identifier);
        }
        let owned_string: String = graph_uri;
        let _ = self.borrow_graphmap_mut().insert(owned_string, new_graph_id);
        Some(new_graph_id)
    }

    /// Add triple to default graph
    fn add_triple(&mut self,
                      triple: (Self::IdentifierType, Self::IdentifierType, Self::IdentifierType))
                      -> Result<(), String> {
        self.add_to_graph(triple.0, triple.1, triple.2, None)
    }

    /// Add to a named graph. If g is None, then add to Default graph.
    fn add_to_graph(&mut self,
                        s: Self::IdentifierType,
                        p: Self::IdentifierType,
                        o: Self::IdentifierType,
                        g: Option<&str>)
                        -> Result<(), String> {
        let guid_g = match g {
            Some(a) => {
                match self.borrow_graphmap().get(a) {
                    Some(guid) => *guid,
                    None => {
                        return Err("That graph does not exist. Create it before adding triples to \
                                    it.".into())
                    }
                }
            }
            None => self.get_default_graph(),
        };
        self.add_to_graph_id(s, p, o, guid_g)
    }
    /// Add to a named graph. If g is None, then add to Default graph.
    fn add_to_graph_id(&mut self,
                           s: Self::IdentifierType,
                           p: Self::IdentifierType,
                           o: Self::IdentifierType,
                           g: Self::GuidType)
                           -> Result<(), String> {
        let guid_s = Self::get_guid_manager_mut().get_or_add_guid(s);
        let guid_p = Self::get_guid_manager_mut().get_or_add_guid(p);
        let guid_o = Self::get_guid_manager_mut().get_or_add_guid(o);
		
		
		let r1 = self.borrow_spog_index_mut().insert(OrdAndEqIndex(Self::IndexSPOGType::create_from_guids(guid_s, guid_p, guid_o, g)));
        let r2 = self.borrow_gp_index_mut().insert(OrdAndEqIndex(Self::IndexGPType::create_from_guids(g, guid_p, guid_s, guid_o)));
        let r3 = self.borrow_ogs_index_mut().insert(OrdAndEqIndex(Self::IndexOGSType::create_from_guids(guid_o, g, guid_s, guid_p)));
        let r4 = self.borrow_pog_index_mut().insert(OrdAndEqIndex(Self::IndexPOGType::create_from_guids(guid_p, guid_o, g, guid_s)));
        let r5 = self.borrow_gsp_index_mut().insert(OrdAndEqIndex(Self::IndexGSPType::create_from_guids(g, guid_s, guid_p, guid_o)));
        let r6 = self.borrow_os_index_mut().insert(OrdAndEqIndex(Self::IndexOSType::create_from_guids(guid_o, guid_s, guid_p, g)));
        if r1 && r2 && r3 && r4 && r5 && r6 {
            Ok(())
        } else {
            Err("Error creating the indexes.".into())
        }
    }

    /// Add to a named graph. If g is None, then add to Default graph.
    fn add_to_all_graphs(&mut self,
                             s: Self::IdentifierType,
                             p: Self::IdentifierType,
                             o: Self::IdentifierType)
                             -> Result<(), String> {
        self.add_to_graph_id(s, p, o, 0u64.into())
    }

    fn get(&self,
           s: Option<Self::IdentifierType>,
           p: Option<Self::IdentifierType>,
           o: Option<Self::IdentifierType>)
           -> Option<Vec<(Rc<Self::IdentifierType>,
                          Rc<Self::IdentifierType>,
                          Rc<Self::IdentifierType>,
                          Rc<Self::IdentifierType>)>> {
        if s.is_none() && p.is_none() && o.is_none() {
            return None;
        }
        self.get_from_graph(s, p, o, None)
    }

    fn get_from_graph(&self,
                      s: Option<Self::IdentifierType>,
                      p: Option<Self::IdentifierType>,
                      o: Option<Self::IdentifierType>,
                      g: Option<&str>)
                      -> Option<Vec<(Rc<Self::IdentifierType>,
                                     Rc<Self::IdentifierType>,
                                     Rc<Self::IdentifierType>,
                                     Rc<Self::IdentifierType>)>> {
        if s.is_none() && p.is_none() && o.is_none() && g.is_none() {
            return None;
        }
        
        let anymatch_guid: Self::GuidType = 0u64.into();

		let guid_g = match g {
            Some(some_g) => {
                match self.borrow_graphmap().get(some_g) {
                    Some(guid) => *guid,
                    None => return None,
                }
            }
            None => self.get_default_graph(),
        };
		
        let graph_rc = match Self::get_guid_manager().lookup_strong_ref(guid_g) {
			Some(a) => a,
			None => return None    
        };
        

        let guid_s = match s {
            Some(ref some_s) => {
                match Self::get_guid_manager().check_existing_guid(some_s) {
                    Some(guid) => *guid,
                    None => return None,
                }
            }
            None => anymatch_guid,
        };
        let s_rc = if guid_s == anymatch_guid { None } else {
		    Self::get_guid_manager().lookup_strong_ref(guid_s)  
        };
        let guid_p = match p {
            Some(ref some_p) => {
                match Self::get_guid_manager().check_existing_guid(some_p) {
                    Some(guid) => *guid,
                    None => return None,
                }
            }
            None => anymatch_guid,
        };
        let p_rc = if guid_p == anymatch_guid { None } else {
		    Self::get_guid_manager().lookup_strong_ref(guid_p)  
        };
        let guid_o = match o {
            Some(ref some_o) => {
                match Self::get_guid_manager().check_existing_guid(some_o) {
                    Some(guid) => *guid,
                    None => return None,
                }
            }
            None => anymatch_guid,
        };
        let o_rc = if guid_o == anymatch_guid { None } else {
		    Self::get_guid_manager().lookup_strong_ref(guid_o)  
        };
        let res = self.borrow_spog_index().range(std::collections::Bound::Included(&OrdAndEqIndex(Self::IndexSPOGType::create_from_guids(guid_s, guid_p, guid_o, 0u64.into()))),
        	std::collections::Bound::Included(&OrdAndEqIndex(Self::IndexSPOGType::create_from_guids(guid_s, guid_p, guid_o, 0u64.into()))));
        for i in res {
        	println!("{:?}", i);
        }
        Some(Vec::new())
        
//        let res = self.borrow_spog_index()
//                      .get_multiple(&OrdAndEqIndex(IndexKeyT::create_from_guids(guid_s, guid_p, guid_o, guid_g)));
//                      
//        match res {
//	        Some(a) => {
//	        	let mut found_nodes = Vec::new();
//	        	
//	        	for found in a {
//		        	let a:(Self::GuidType,Self::GuidType,Self::GuidType,Self::GuidType)
//		        	    = found.0.get_compare_guids_tuple();
//		        	if a.3 != guid_g { panic!("Got a non-matching Graph id when specifying a specific Graph in get_from_graph!"); }
//		        	let new_graph_rc = graph_rc.clone();
//		        	let new_s_rc = match s_rc { Some(ref a) => a.clone(),
//		        		None => Self::get_guid_manager().lookup_strong_ref(a.0).unwrap()
//		        	};
//		        	let new_p_rc = match p_rc { Some(ref a) => a.clone(),
//		        		None => Self::get_guid_manager().lookup_strong_ref(a.1).unwrap()
//		        	};
//		        	let new_o_rc = match o_rc { Some(ref a) => a.clone(),
//		        		None => Self::get_guid_manager().lookup_strong_ref(a.2).unwrap()
//		        	};
//		        	found_nodes.push((new_s_rc, new_p_rc, new_o_rc, new_graph_rc))    
//	        	}
//	        	Some(found_nodes)
//	        },
//	        None => None
//        }              
    }
    fn get_from_all_graphs(&self,
                           s: Option<Self::IdentifierType>,
                           p: Option<Self::IdentifierType>,
                           o: Option<Self::IdentifierType>)
                           -> Option<Vec<(Rc<Self::IdentifierType>,
                                          Rc<Self::IdentifierType>,
                                          Rc<Self::IdentifierType>,
                                          Rc<Self::IdentifierType>)>> {
        if s.is_none() && p.is_none() && o.is_none() {
            return None;
        }
        let default_guid: Self::GuidType = 0u64.into();

        let guid_s = match s {
            Some(ref some_s) => {
                match Self::get_guid_manager().check_existing_guid(some_s) {
                    Some(guid) => *guid,
                    None => default_guid,
                }
            }
            None => default_guid,
        };
        let guid_p = match p {
            Some(ref some_p) => {
                match Self::get_guid_manager().check_existing_guid(some_p) {
                    Some(guid) => *guid,
                    None => default_guid,
                }
            }
            None => default_guid,
        };
        let guid_o = match o {
            Some(ref some_o) => {
                match Self::get_guid_manager().check_existing_guid(some_o) {
                    Some(guid) => *guid,
                    None => default_guid,
                }
            }
            None => default_guid,
        };
        let res = self.borrow_spog_index().range(std::collections::Bound::Included(&OrdAndEqIndex(Self::IndexSPOGType::create_from_guids(guid_s, guid_p, guid_o, 0u64.into()))),
        	std::collections::Bound::Included(&OrdAndEqIndex(Self::IndexSPOGType::create_from_guids(guid_s, guid_p, guid_o, 0u64.into()))));
        for i in res {
        	println!("{:?}", i);
        }
        
        //match res {
        //    Some(a) => println!("{:?}", a),
        //    None => println!("Got None!"),
        //};
        Some(Vec::new())
    }
    
}

//impl<A: fmt::Display+fmt::Debug+PartialOrd+Ord, B: Clone+Copy+fmt::Display+fmt::Debug+From<u64>+Into<u64>, C:IndexKeyT<A=A, B=B>> TripleStore<A, B, C> {
   
    
//}

//impl<A: fmt::Display+fmt::Debug+PartialOrd+Ord, B: Clone+Copy+fmt::Display+fmt::Debug+From<u64>+Into<u64>, T: TripleStoreT<IdentifierType=A, GuidType=B>> Default for T {
//	fn default() -> TripleStore<A, B> {
//		let index_spog: BTreeSet<OrdAndEqIndex> = BTreeSet::new();
//        let index_gp: BTreeSet<OrdAndEqIndex<C>> = BTreeSet::new();
//        let index_ogs: BTreeSet<OrdAndEqIndex<C>> = BTreeSet::new();
//        let index_pog: BTreeSet<OrdAndEqIndex<C>> = BTreeSet::new();
//        let index_gsp: BTreeSet<OrdAndEqIndex<C>> = BTreeSet::new();
//        let index_os: BTreeSet<OrdAndEqIndex<C>> = BTreeSet::new();
//        
//        let mut new_qs = TripleStore {
//            indexes: [index_spog, index_gp, index_ogs, index_pog, index_gsp, index_os],
//            default_graph: 0u64.into(),
//            graphs: HashMap::new(),
//        };
//        let graph_id = new_qs.add_new_graph("DefaultGraph");
//        new_qs.default_graph = graph_id.unwrap();
//        new_qs
//	}
//}

impl<T: IndexKeyT + PartialEq> PartialEq for OrdAndEqIndex<T>
    where <T as IndexKeyT>::B: PartialEq
{
    fn eq(&self, rhs_type: &Self) -> bool {
        let lhs = self.0.get_compare_guids_tuple();
        let rhs = rhs_type.0.get_compare_guids_tuple();
        lhs.0 == rhs.0 && lhs.1 == rhs.1 && lhs.2 == rhs.2 && lhs.3 == rhs.3
    }
}

impl<T: IndexKeyT + Eq> Eq for OrdAndEqIndex<T> where <T as IndexKeyT>::B: Eq {}



#[cfg(test)]
mod tests {
    use std::rc::Rc;
    use btree::map::BTreeMap;
    use btree::set::BTreeSet;
    use rdf_triple_store::{RDFIdentifier, RDFUnit};
    use rdf_triple_store::IndexKeySPOG;
    use IndexKeyT;
    use OrdAndEqIndex;
    use std::marker::PhantomData;
    use std::cmp::{Ord, PartialOrd, Eq, PartialEq};
    use rdf_triple_store::TripleStore;
    use TripleStoreT;

    #[test]
    fn test() {
        let mut store: TripleStore<RDFIdentifier, RDFUnit> = TripleStore::new();
        let rdf_me: RDFIdentifier = String::from("http://example.org/id/ashley").into();
        let rdf_favcolor: RDFIdentifier = String::from("http://example.org/id/favorite_color").into();
        let rdf_color_blue: RDFIdentifier = String::from("http://example.org/id/blue").into();
        let rdf_color_red: RDFIdentifier = String::from("http://example.org/id/red").into();
        let rdf_wife: RDFIdentifier = String::from("http://example.org/id/wife").into();
        let rdf_marriedto: RDFIdentifier = String::from("http://example.org/id/married_to").into();
        store.add_triple((rdf_me.clone(), rdf_favcolor.clone(), rdf_color_blue));
        store.add_triple((rdf_me.clone(), rdf_marriedto, rdf_wife.clone()));
        store.add_triple((rdf_wife.clone(), rdf_favcolor, rdf_color_red));
        //store.print_resolver();
        let rdf_testval: RDFIdentifier = String::from("http://example.org/id/testval").into();
        for i in 1..100 {
	        let rdf_id: RDFIdentifier = format!("http://example.org/id/teststring/{}",i).into();
	        store.add_triple((rdf_me.clone(), rdf_testval.clone(), rdf_id));
        }

        //let res = store.get(Some(rdf_me.clone()), None, None);
        let res = store.get(Some(rdf_me), Some(rdf_testval), None);
        let res2 = store.get(Some(rdf_wife), None, None);

        print!("{:?}", res);

    }
}
