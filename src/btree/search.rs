// Copyright 2015 The Rust Project Developers. See the COPYRIGHT
// file at the top-level directory of this distribution and at
// http://rust-lang.org/COPYRIGHT.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::cmp::Ordering;

use std::borrow::Borrow;

use super::node::{Handle, NodeRef, marker};

use super::node::ForceResult::*;


pub enum SearchResult<BorrowType, K, V, FoundType, GoDownType> {
    Found(Handle<NodeRef<BorrowType, K, V, FoundType>, marker::KV>),
    GoDown(Handle<NodeRef<BorrowType, K, V, GoDownType>, marker::Edge>)
}


pub enum SearchResultMultiple<'a, K, V, FoundType, GoDownType> {
    Found(Vec<Handle<NodeRef<marker::Immut<'a>, K, V, FoundType>, marker::KV>>),
    #[allow(type_complexity)]
    GoDown(Option<Vec<Handle<NodeRef<marker::Immut<'a>, K, V, FoundType>, marker::KV>>>, Handle<NodeRef<marker::Immut<'a>, K, V, GoDownType>, marker::Edge>)
}

pub fn search_tree<BorrowType, K, V, Q: ?Sized>(
    mut node: NodeRef<BorrowType, K, V, marker::LeafOrInternal>,
    key: &Q
) -> SearchResult<BorrowType, K, V, marker::LeafOrInternal, marker::Leaf>
        where Q: Ord, K: Borrow<Q> {

    loop {
        match search_node(node, key) {
            SearchResult::Found(handle) => return SearchResult::Found(handle),
            SearchResult::GoDown(handle) => match handle.force() {
                Leaf(leaf) => return SearchResult::GoDown(leaf),
                Internal(internal) => {
                    node = internal.descend();
                    continue;
                }
            }
        }
    }
}
        
pub fn search_tree_multiple<'a, K: 'a, V: 'a, Q: ?Sized>(
    mut node: NodeRef<marker::Immut<'a>, K, V, marker::LeafOrInternal>,
    key: &Q
) -> SearchResultMultiple<'a, K, V, marker::LeafOrInternal, marker::Leaf>
        where Q: Ord, K: Borrow<Q> {
	let mut found_in_tree = Vec::new();
    loop {
        match search_node_multiple(node, key) {
            SearchResultMultiple::Found(mut handle) => { found_in_tree.append(&mut handle); return SearchResultMultiple::Found(found_in_tree); },
            SearchResultMultiple::GoDown(kv_handles, edge_handle) => {
            	if let Some(mut a) = kv_handles {
	            	found_in_tree.append(&mut a)
            	}
            	
            	match edge_handle.force() {
	                Leaf(leaf) =>  {
	                	if found_in_tree.len() < 1 {
	                		return SearchResultMultiple::GoDown(None, leaf);
	                	} else {
		                	return SearchResultMultiple::Found(found_in_tree);
	                	}
	                },
	                Internal(internal) => {
	                    node = internal.descend();
	                    continue;
	                }
            	} 
            }
        }
    }
}

pub fn search_node<BorrowType, K, V, Type, Q: ?Sized>(
    node: NodeRef<BorrowType, K, V, Type>,
    key: &Q
) -> SearchResult<BorrowType, K, V, Type, Type>
        where Q: Ord, K: Borrow<Q> {

    match search_linear(&node, key) {
        (idx, true) => SearchResult::Found(
            Handle::new_kv(node, idx)
        ),
        (idx, false) => SearchResult::GoDown(
            Handle::new_edge(node, idx)
        )
    }
}
        
pub fn search_node_multiple<'a, K: 'a, V: 'a, Type: 'a, Q: ?Sized>(
    node: NodeRef<marker::Immut<'a>, K, V, Type>,
    key: &Q
) -> SearchResultMultiple<'a, K, V, Type, Type>
        where Q: Ord, K: Borrow<Q> {
	let mut found_kvs = Vec::new();
    let (found_indexes, idx) = search_linear_multiple(&node, key);
    
	if found_indexes.len() < 1 {
		return SearchResultMultiple::GoDown( None, Handle::new_edge(node, idx) );
	}
	
	for found_index in &found_indexes {
    	found_kvs.push(Handle::new_kv(node.clone(), *found_index)); //TODO: Node.clone() may not belong here, maybe node is always a copy type
	}
	
	SearchResultMultiple::GoDown( Some(found_kvs), Handle::new_edge(node, idx) )
      
}           

fn search_linear<BorrowType, K, V, Type, Q: ?Sized>(
    node: &NodeRef<BorrowType, K, V, Type>,
    key: &Q
) -> (usize, bool)
        where Q: Ord, K: Borrow<Q> {
    for (i, k) in node.keys().iter().enumerate() {
        match key.cmp(k.borrow()) {
            Ordering::Greater => {},
            Ordering::Equal => return (i, true),
            Ordering::Less => return (i, false)
        }
    }
    (node.keys().len(), false)
}


fn search_linear_multiple<BorrowType, K, V, Type, Q: ?Sized>(
    node: &NodeRef<BorrowType, K, V, Type>,
    key: &Q
) -> (Vec<usize>, usize)
        where Q: Ord, K: Borrow<Q> {
	let mut found_indexes: Vec<usize> = Vec::with_capacity(node.len());
    for (i, k) in node.keys().iter().enumerate() {
        match key.cmp(k.borrow()) {
            Ordering::Greater => {},
            Ordering::Equal => { found_indexes.push(i); },
            Ordering::Less => return (found_indexes, i)
        }
    }
    (found_indexes, node.keys().len())
}

